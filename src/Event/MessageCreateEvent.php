<?php

namespace App\Event;


use App\Entity\Message;
use App\Repository\ThreadRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\EventDispatcher\Event;

class MessageCreateEvent extends Event
{
    const NAME = 'message.create';

    /**
     * @var Message
     */
    private $createdMessage;
    private $entityManager;
    private $threadRepository;

    public function __construct(Message $createdMessage, EntityManagerInterface $entityManager, ThreadRepository $threadRepository)
    {
        $this->createdMessage = $createdMessage;
        $this->entityManager = $entityManager;
        $this->threadRepository = $threadRepository;
    }

    /**
     * @return Message
     */
    public function getCreatedMessage(): Message
    {
        return $this->createdMessage;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return ThreadRepository
     */
    public function getThreadRepository(): ThreadRepository
    {
        return $this->threadRepository;
    }
}