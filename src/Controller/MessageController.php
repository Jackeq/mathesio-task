<?php

namespace App\Controller;

use App\Entity\Message;
use App\Entity\Thread;
use App\Entity\User;
use App\Event\MessageCreateEvent;
use App\Form\MessageType;
use App\Repository\ThreadRepository;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageController extends AbstractController
{
    /**
     * @Route("/{id}/create-message", name="create-message")
     * @param Thread $thread
     * @param Request $request
     * @param EventDispatcherInterface $eventDispatcher
     * @param ThreadRepository $threadRepository
     * @param EntityManagerInterface $em
     * @return Response
     * @throws Exception
     */
    public function createMessage(Thread $thread,
                                  Request $request,
                                  EventDispatcherInterface $eventDispatcher,
                                  ThreadRepository $threadRepository,
                                  EntityManagerInterface $em): Response
    {

        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // If User authentication was implemented: $thread->setAuthor($this->getUser());
            $user = $this->getDoctrine()->getRepository(User::class)->find(1);
            $message->setAuthor($user);

            $message->setDate(new DateTime());
            $message->setThread($thread);

            $em->persist($message);
            $em->flush();

            /* EventListener not working
            $messageCreateEvent = new MessageCreateEvent($message, $em, $threadRepository);
            $eventDispatcher->dispatch($messageCreateEvent, MessageCreateEvent::NAME);
            */
            return $this->redirectToRoute('view-thread', [
                'id' => $thread->getId()
            ]);
        }

        return $this->render('message/create.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
