<?php

namespace App\Controller;

use App\Entity\Thread;
use App\Entity\User;
use App\Form\ThreadType;
use App\Repository\ThreadRepository;
use DateTime;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ThreadController extends AbstractController
{
    /**
     * @Route("/", name="threads")
     * @param ThreadRepository $threadRepository
     * @return Response
     */
    public function index(ThreadRepository $threadRepository)
    {
        $threads = $threadRepository->findAll();
        return $this->render('thread/index.html.twig', [
            'threads' => $threads
        ]);
    }

    /**
     * @Route("/new-thread", name="create-thread")
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function createThread(Request $request): Response
    {
        $thread = new Thread();
        $form = $this->createForm(ThreadType::class, $thread);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // If User authentication was implemented: $thread->setAuthor($this->getUser());
            $user = $this->getDoctrine()->getRepository(User::class)->find(1);
            $thread->setAuthor($user);

            $thread->setDate(new DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($thread);
            $em->flush();

            return $this->redirectToRoute('threads');
        }

        return $this->render('thread/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/thread/{id}", name="view-thread")
     * @ParamConverter("thread", class="App\Entity\Thread")
     * @param Thread $thread
     * @return Response
     */
    public function viewThread(Thread $thread): Response
    {
        $messages = $thread->getMessages();

        return $this->render('thread/view.html.twig', [
            'messages' => $messages,
            'thread' => $thread
        ]);
    }
}
