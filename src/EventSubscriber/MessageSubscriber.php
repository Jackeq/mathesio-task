<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Event\MessageCreateEvent;

class MessageSubscriber implements EventSubscriberInterface
{
    public function onMessageCreate(MessageCreateEvent $event)
    {
        $message = $event->getCreatedMessage();
        $threadRepository = $event->getThreadRepository();
        $threadAuthor = $threadRepository->find($message->getThread()->getAuthor());
        $notification = $threadAuthor->getNotification();
        $notify = 'New message in thread: ' . $message->getThread()->getName();
        array_push($notification, $notify);


        $threadAuthor->setNotification($notification);

        $entityManager = $event->getEntityManager();
        $entityManager->persist($threadAuthor);
        $entityManager->flush();
    }

    public static function getSubscribedEvents()
    {
        return [
            'message.create' => 'onMessageCreate',
        ];
    }
}
